package com.extweb4j.web.controller;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.extweb4j.core.controller.ExtController;
import com.extweb4j.core.kit.ExtKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;
/**
 * 
 * @author: 周高军 2016年5月18日
 */
public class UploadController extends ExtController{
	/**
	 * kindhtml 文件上传
	 */
	public void uploadFile(){
		UploadFile uploadFile = getFile();
		try {
			
			if(uploadFile.getFile().length() > (1024 * 1024 * 20)){
				renderJson(new Record().set("error", 1).set("message", "上传文件大小不能超过10Mb!"));
				return;
			}
			
			String path = PropKit.get("uploadFile");
			String webAddr = PropKit.get("webAddr");
			String backupFileDir = PropKit.get("backupFileDir");
			String fileName = ExtKit.renameFile(uploadFile.getFileName());
			
			FileUtils.copyFile(uploadFile.getFile(),new File(path+fileName));
			//备份
			FileUtils.copyFile(uploadFile.getFile(),new File(backupFileDir + fileName));
			
			String fileUrl = webAddr + fileName.replaceAll("\\\\","/");
			
			System.out.println(fileUrl);
			
			renderJson(new Record().set("error", 0).set("url", fileUrl));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			renderJson(new Record().set("error", 1).set("message", "上传出错!"));
		}
	}
	/**
	 * 文件管理
	 */
	public void fileManager(){
		
	}
}
