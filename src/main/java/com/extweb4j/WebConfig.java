package com.extweb4j;

import com.extweb4j.core.interceptor.ExtInterceptor;
import com.extweb4j.web.plugin.ExtActiveRecordPlugin;
import com.extweb4j.web.route.ExtRoutes;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;

/**
 * @ClassName: WebConfig SHFT+ALT+J
 * @Description: TODO
 * @author: 周高军
 * @date: 2016年1月5日 上午11:42:54
 */
public class WebConfig extends JFinalConfig {

	static {
		PropKit.use("web.properties");
	}

	/**
	 * 配置常量
	 */
	public void configConstant(Constants me) {
		me.setViewType(ViewType.FREE_MARKER);
		me.setEncoding("UTF-8");
		me.setDevMode(PropKit.getBoolean("devMode", false));
		me.setBaseViewPath("/WEB-INF/views");
		//上传文件最大20M
		me.setMaxPostSize(1024 * 1024 * 21);
	}

	/**
	 * 配置路由
	 */
	public void configRoute(Routes me) {
		me.add(new ExtRoutes());
	}

	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me) {
		//mysql
		
		DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbcUrl"),
				PropKit.get("user"), PropKit.get("password"));
		me.add(druidPlugin);
		ExtActiveRecordPlugin arp = new ExtActiveRecordPlugin(druidPlugin);
		arp.setDialect(new MysqlDialect());
		arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
		me.add(arp);
		
		//oracle
		
		/*DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("orcl_jdbcUrl"),
				PropKit.get("orcl_user"), PropKit.get("orcl_password"),
				PropKit.get("orcl_driverClass"));
		me.add(druidPlugin);
		AdminActiveRecordPlugin arp = new AdminActiveRecordPlugin(druidPlugin);
		arp.setDialect(new OracleDialect());
		arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
		me.add(arp);*/

		me.add(new EhCachePlugin());
	}

	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me) {
		me.add(new ExtInterceptor());
	}

	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me) {

	}

	@Override
	public void afterJFinalStart() {
		// TODO Auto-generated method stub
		super.afterJFinalStart();
		//System.out.println(TblNewsType.dao.findById("1").get("sort"));
	}
	
	
}
