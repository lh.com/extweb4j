package com.extweb4j.core.daoaload;

import java.util.List;

import com.extweb4j.core.model.ExtMenu;
import com.jfinal.plugin.ehcache.IDataLoader;
/**
 * 更具keys获取数据组和数据项
 * @author: 周高军 2016年6月2日
 */
public class ActionAuthDataLoader  implements IDataLoader{
	
	private String uid;
	
	@Override
	public Object load() {
		List<ExtMenu> menus  = ExtMenu.dao.findActionBy(uid);
		
		return menus;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public ActionAuthDataLoader() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ActionAuthDataLoader(String uid) {
		super();
		this.uid = uid;
	}
	
	
}
