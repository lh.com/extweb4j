Ext.define('Admin.view.system.log.Log', {
	extend: 'Admin.ux.PageGrid',
    xtype: 'log-view',
    title: '系统日志',
    viewModel: {
       stores: {
	        data: {
	            type: 'log-store',
	            autoLoad: true
	   		 }
	    }
    },
 	columns: [
	 	{ xtype: 'rownumberer',width:80,text:'行号'},
	 	{ text: '用户', dataIndex: 'user_name'}, 
	 	{ text: '创建时间', dataIndex: 'create_time',width:130},
	 	{ text: '用户操作', dataIndex: 'log_url',flex:1,	renderer :function (value, meta, record) {
	           meta.tdAttr = 'data-qtip="' + record.get('log_params') + '"';
	           return value;
	        }
	    }
	    ],
	    tbar:[
	    	{
				xtype : 'keysearchfield'
			}
	    ]
});
