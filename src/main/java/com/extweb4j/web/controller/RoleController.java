package com.extweb4j.web.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.extweb4j.core.anno.AuthAnno;
import com.extweb4j.core.controller.ExtController;
import com.extweb4j.core.enums.RowStatus;
import com.extweb4j.core.kit.ExtKit;
import com.extweb4j.core.model.ExtMenu;
import com.extweb4j.core.model.ExtRole;
import com.extweb4j.core.model.ExtRoleMenu;
import com.extweb4j.web.bean.TreeBean;
import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.tx.Tx;
/**
 * 角色控制器
 * @author Administrator
 *
 */
public class RoleController extends ExtController{
	
	/**
	 * 列表
	 */
	@AuthAnno
	public void list(){
		int page = getParaToInt("page");
		int limit = getParaToInt("limit");
		String keywords = getPara("keywords");
		String where = "row_status != ?";
		Object[] parmas = new Object[]{2};
		String sort = "create_time";
		Page<ExtRole> pageData = ExtRole.dao.pageDataBy(page,limit,keywords,sort,"ASC",where,parmas);
		renderJson(pageData);
	}
	/**
	 * 新增
	 */
	@AuthAnno
	@Before(Tx.class)
	public void add(){
		//新增用户
		ExtRole role = getExtModel(ExtRole.class);
		role.set("id",ExtKit.UUID());
		role.set("create_time",new Timestamp(new Date().getTime()));
		role.save();
		success();
	}
	/**
	 * 编辑
	 */
	@AuthAnno
	@Before(Tx.class)
	public void edit(){
		ExtRole role = getExtModel(ExtRole.class);
		role.update();
		success();
	}
	/**
	 * 逻辑删除
	 */
	@AuthAnno
	@Before(Tx.class)
	public void delete(){
		ExtRole role = ExtRole.dao.findById(getPara("id"));
		if(role == null){
			throw new RuntimeException("要删除的角色不存在");
		}
		role.set("row_status",RowStatus.删除.getState());
		role.update();
		success();
	}
	/**
	 * 获取所有可用角色
	 */
	public void json(){
		List<ExtRole> list = ExtRole.dao.findByExcludeAttr("row_status" ,RowStatus.删除.getState() );
		renderJson(list);
	}
	/**
	 * 获取权限
	 */
	@AuthAnno
	public void auth(){
		String roleId = getPara("roleId");
		List<TreeBean> treeBeans  = new ArrayList<TreeBean>();
		String defaultPid = "0";
		treeBeans = this.searchTreeNodes(defaultPid,roleId);
		renderJson(treeBeans);
	}
	
	/**
	 * 递归获取节点
	 * @param pid
	 * @return
	 */
	
	private List<TreeBean> searchTreeNodes(String pid,String roleId){
		
		List<TreeBean> treeBeans = new ArrayList<TreeBean>();
		
		List<ExtMenu> menuTree = ExtMenu.dao.getAuthMenu(pid,roleId);
		
		for(ExtMenu menu : menuTree){
			TreeBean bean = new TreeBean();
			bean.setId(menu.getStr("id"));
			bean.setText(menu.getStr("text"));
			bean.setChecked(menu.getBoolean("checked"));
			bean.setExpanded(true);
			boolean leaf =(menu.getStr("leaf")!=null && menu.getStr("leaf").equals("1")) ? true:false;
			bean.setLeaf(leaf);
			if(!leaf){
				bean.setChildren(searchTreeNodes(menu.getStr("id"),roleId));
			}
			treeBeans.add(bean);
		}
		return treeBeans;
	}
	
	/**
	 * 保存权限
	 */
	@Before(Tx.class)
	public void doAuth(){
		
		String  roleId = getPara("roleId");
		String[] menuIds = getParaValues("menuIds");
		
		if(StringUtils.isBlank(roleId)){
			throw new RuntimeException("选择角色为空");
		}
		ExtRoleMenu.dao.deleteByAttr("role_id", roleId);
		
		if(ArrayUtils.isNotEmpty(menuIds)){
			for(String mid : menuIds){
				if(mid!=null && !mid.equals("root")){
					ExtRoleMenu erm = new ExtRoleMenu();
					erm.set("id",ExtKit.UUID());
					erm.set("role_id",roleId);
					erm.set("menu_id", mid);
					erm.save();
				}
			}
		}
		success("权限分完成");
	}
}
