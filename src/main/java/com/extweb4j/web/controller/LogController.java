package com.extweb4j.web.controller;

import com.extweb4j.core.anno.AuthAnno;
import com.extweb4j.core.controller.ExtController;
import com.extweb4j.core.model.ExtLog;
import com.jfinal.plugin.activerecord.Page;
/**
 * 日志控制器
 * @author Administrator
 *
 */
public class LogController extends ExtController{
	
	@AuthAnno
	public void list(){
		int page = getParaToInt("page");
		int limit = getParaToInt("limit");
		String keywords = getPara("keywords");
		String sort = "create_time";
		Page<ExtLog> pageData = ExtLog.dao.pageDataBy(page,limit,keywords,sort,"ASC");
		renderJson(pageData);
	}
	
}
