Ext.apply(Ext,{
	//应用显示名称
	appName : "WEB快速开发平台-ExtWeb4j",
	//提示信息
	info: function(msg){
		Ext.toast({
		     html: "<font color='green'>"+msg+"!</font>",
		     title: '消息',
		     width: 300,
		     align: 't'
		 });
	},
	//错误消息
	error: function(msg){
		Ext.toast({
		     html: "<font color='red'>"+msg+"!</font>",
		     title: '错误',
		     width: 300,
		     align: 't'
		 });
	}
});