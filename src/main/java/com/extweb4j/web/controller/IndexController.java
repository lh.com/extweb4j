package com.extweb4j.web.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import cn.dreampie.captcha.CaptchaRender;

import com.extweb4j.core.bean.Msg;
import com.extweb4j.core.comstant.CacheConstant;
import com.extweb4j.core.controller.ExtController;
import com.extweb4j.core.daoaload.ActionAuthDataLoader;
import com.extweb4j.core.kit.ExtKit;
import com.extweb4j.core.kit.LoginKit;
import com.extweb4j.core.model.ExtMenu;
import com.jfinal.aop.Clear;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.upload.UploadFile;
/**
 * 
 * @author: 周高军 2016年5月18日
 */
@Clear
public class IndexController extends ExtController{
	/**
	 * 导航页
	 */
	public void index(){
		//setSessionAttr("uid","7b7c4d46de6744f99a0031ed01f4346f");
	}
	/**
	 * 登录导航页
	 */
	public void login(){
		redirect("#login");
	}
	/**
	 * 执行登录
	 */
	public void doLogin(){
	    try {
			LoginKit.login(getRequest());
			success();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			renderJson(new Msg(false, ExtKit.formatException(e)));
		}
	}
	/**
	 * 登出控制器
	 */
	public void logout(){
		LoginKit.logout(getRequest());
		redirect("#login");
	}
	/**
	 * 验证是否登录
	 */
	public void isLogin(){
		renderJson(new Msg(LoginKit.isLogin(getRequest())));
	}
	/**
	 * 验证码
	 */
	public void captcha(){
		CaptchaRender captcha = new CaptchaRender();
		captcha.setCode("0123456789");
		captcha.setRandomColor(true);
		captcha.setImgSize(150, 45);
		//captcha.setDrawColor(new Color(0x35baf6));
		//captcha.setDrawBgColor(new Color(0x35baf6));
		captcha.setCaptchaName("captcha");
		captcha.setFontNum(4, 6);
		render(captcha);
	}
	
	/**
	 * 获取当前用户
	 */
	
	public void getUser(){
		renderJson(new Msg(true, "OK",LoginKit.getSessionUser(getRequest())));
	}
	
	/**
	 * wangHtml 上传图片
	 */
	@Deprecated
	public void uploadImg(){
		UploadFile uploadFile = getFile();
		try {
			String path = PropKit.get("uploadImg");
			FileUtils.copyFile(uploadFile.getFile(),new File(path+uploadFile.getFileName()));
			renderText("https://www.baidu.com/img/bd_logo1.png");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			renderText("error|服务器端错误");
		}
	}
	/**
	 * 验证改用户是否有权限
	 */
	
	public void hasAuth(){

		String action = getPara("action");
		String uid = getSessionAttr("uid");
		if(uid==null){
			throw new RuntimeException("登录过期,请重新登录");
		}
		
		List<String> auths = new ArrayList<String>();
		List<ExtMenu> list = CacheKit.get(CacheConstant.GLOBAL_CACHE_NAME,(uid+"_action"), new ActionAuthDataLoader(uid));
		for(ExtMenu m : list){
			auths.add(m.getStr("action"));
		}
		if(!auths.contains(action)){
			throw new RuntimeException("没有权限,"+action);
		}
		success();
	}
	
}
